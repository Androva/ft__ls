# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/09/05 14:55:37 by nmatutoa          #+#    #+#              #
#    Updated: 2018/09/10 11:29:40 by nmatutoa         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls
FLAGS = gcc -Wall -Werror -Wextra
SOURCES = count_size.c flags_init.c ft_ls.c get_files.c get_permissions.c l_flag.c  no_flags.c \
		lstadd.c lstnew.c lstdel.c swap.c lstsort.c diradd.c dirdel.c dirnew.c dirsort.c \

OBJECTS = $(SOURCES:.c=.o)

all: $(NAME)

$(NAME):
	@make -C libft
	@$(FLAGS) -c $(SOURCES)
	@$(FLAGS) -o $(NAME) $(OBJECTS) -L libft/ -lft
	@echo "Executable 'ft_ls' created"

clean:
	@/bin/rm -f $(OBJECTS)
	@make clean -C libft
	@echo "ft_ls object files removed"

fclean: clean
	@/bin/rm -f ft_ls
	@make fclean -C libft
	@echo "Executable ft_ls removed"
	@echo "libft.a removed"

re: fclean all
