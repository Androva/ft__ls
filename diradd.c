/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   diradd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/06 12:13:22 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/06 13:52:09 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	diradd(t_dir **lst, t_dir *new)
{
	t_dir	*current;

	current = *lst;
	if (*lst == NULL)
		*lst = new;
	else
	{
		while (current->next != NULL)
			current = current->next;
		current->next = new;
	}
}
