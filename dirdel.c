/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dirdel.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/10 10:58:02 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/10 14:57:35 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_dir	*dirdel(t_dir **head)
{
	t_dir	*dir;
	t_dir	*temp;

	dir = *head;
	while (dir != NULL)
	{
		temp = dir;
		dir = dir->next;
		if (temp->directory != NULL)
			free(temp->directory);
		free(temp);
		temp = NULL;
	}
	return (NULL);
}
