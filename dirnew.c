/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dirnew.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/06 12:10:39 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/10 15:35:37 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_dir		*dirnew(char *directory)
{
	t_dir	*list;

	if (!(list = (t_dir *)ft_memalloc(sizeof(t_dir))))
		return (NULL);
	if (directory)
		list->directory = ft_strdup(directory);
	else
		list->directory = NULL;
	list->next = NULL;
	return (list);
}
