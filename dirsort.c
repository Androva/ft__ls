/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dirsort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/06 12:14:53 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/10 15:48:36 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	dirsort(t_dir *list)
{
	t_dir	*head;
	t_dir	*comp;

	head = list;
	while (list != NULL)
	{
		comp = list->next;
		while (comp != NULL)
		{
			if (ft_strcmp(list->directory, comp->directory) > 0)
			{
				swap(&list->directory, &comp->directory);
				list = head;
				comp = list->next;
			}
			else
				comp = comp->next;
		}
		list = list->next;
	}
}

void	dirrsort(t_dir *list)
{
	t_dir	*head;
	t_dir	*comp;

	head = list;
	while (list != NULL)
	{
		comp = list->next;
		while (comp != NULL)
		{
			if (ft_strcmp(list->directory, comp->directory) < 0)
			{
				swap(&list->directory, &comp->directory);
				list = head;
				comp = list->next;
			}
			else
				comp = comp->next;
		}
		list = list->next;
	}
}

void	sortdir(t_dir *dir, t_flags flags)
{
	if (flags.r != 0)
		dirrsort(dir);
	else
		dirsort(dir);
}
