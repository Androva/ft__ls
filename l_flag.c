/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   l_flag.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/01 11:24:06 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/10 15:52:13 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_files	*get_stats(t_files *files, char *directory)
{
	t_files		*head;
	struct stat	info;
	char		*c_time;
	char		*path;

	head = files;
	while (files != NULL)
	{
		path = NULL;
		path = ft_strjoin(directory, files->filename);
		c_time = NULL;
		lstat(path, &info);
		files->links = info.st_nlink;
		files->size = info.st_size;
		files->blocks = info.st_blocks;
		c_time = ft_strdup(ctime(&info.st_mtime));
		files->time = ft_strnew(12);
		ft_strncpy(files->time, &*(c_time + 4), 12);
		free(c_time);
		files = files->next;
		free(path);
	}
	return (head);
}

t_files	*get_user(t_files *files, char *directory)
{
	t_files			*head;
	struct stat		info;
	struct passwd	*user;
	char			*path;

	head = files;
	while (files != NULL)
	{
		files->user = NULL;
		path = NULL;
		path = ft_strjoin(directory, files->filename);
		lstat(path, &info);
		free(path);
		user = getpwuid(info.st_uid);
		files->user = ft_strdup(user->pw_name);
		files = files->next;
	}
	return (head);
}

t_files	*get_group(t_files *files, char *directory)
{
	t_files			*head;
	struct stat		info;
	struct group	*group;
	char			*path;

	head = files;
	while (files != NULL)
	{
		path = NULL;
		path = ft_strjoin(directory, files->filename);
		lstat(path, &info);
		free(path);
		group = getgrgid(info.st_gid);
		files->group = ft_strdup(group->gr_name);
		files = files->next;
	}
	return (head);
}

t_files	*get_perm(t_files *files, char *directory)
{
	t_files		*head;
	struct stat	info;
	char		*path;

	head = files;
	while (files != NULL)
	{
		path = NULL;
		path = ft_strjoin(directory, files->filename);
		files->permissions = NULL;
		files->permissions = ft_strnew(10);
		lstat(path, &info);
		free(path);
		if (S_ISDIR(info.st_mode))
			files->permissions[0] = 'd';
		else
			files->permissions[0] = '-';
		ft_strcpy(files->permissions, user_perm(info, files->permissions));
		ft_strcpy(files->permissions, group_perm(info, files->permissions));
		ft_strcpy(files->permissions, other_perm(info, files->permissions));
		files = files->next;
	}
	return (head);
}

void	l_flag(t_files *files, char *directory)
{
	int count;

	files = get_stats(files, directory);
	files = get_user(files, directory);
	files = get_group(files, directory);
	files = get_perm(files, directory);
	count = count_size(files);
	if (files != NULL)
	{
		ft_putstr("total ");
		ft_putendldel(ft_itoa(count));
	}
	while (files != NULL)
	{
		ft_padstr(files->permissions, 11);
		ft_padstrdel(ft_itoa(files->links), -3);
		ft_putchar(' ');
		ft_padstr(files->user, 10);
		ft_padstr(files->group, 9);
		ft_padstrdel(ft_itoa(files->size), -7);
		ft_putchar(' ');
		ft_padstr(files->time, 13);
		ft_putendl(files->filename);
		files = files->next;
	}
}
