/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lstnew.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/01 11:48:39 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/10 13:36:28 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_files		*lstnew(char *filename)
{
	t_files	*list;

	if (!(list = (t_files *)ft_memalloc(sizeof(t_files))))
		return (NULL);
	if (filename)
		list->filename = ft_strdup(filename);
	else
		list->filename = NULL;
	list->user = NULL;
	list->group = NULL;
	list->permissions = NULL;
	list->time = NULL;
	list->next = NULL;
	return (list);
}
