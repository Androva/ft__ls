/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lstsort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/01 14:18:57 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/07 11:13:55 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	lstsort(t_files *list)
{
	t_files	*head;
	t_files	*comp;

	head = list;
	while (list != NULL)
	{
		comp = list->next;
		while (comp != NULL)
		{
			if (ft_strcmp(list->filename, comp->filename) > 0)
			{
				swap(&list->filename, &comp->filename);
				list = head;
				comp = list->next;
			}
			else
				comp = comp->next;
		}
		list = list->next;
	}
}

void	rlstsort(t_files *list)
{
	t_files	*head;
	t_files	*comp;

	head = list;
	while (list != NULL)
	{
		comp = list->next;
		while (comp != NULL)
		{
			if (ft_strcmp(list->filename, comp->filename) < 0)
			{
				swap(&list->filename, &comp->filename);
				list = head;
				comp = list->next;
			}
			else
				comp = comp->next;
		}
		list = list->next;
	}
}

void	tlstsort(t_files *list, t_files *head)
{
	t_files		*comp;
	struct stat	info1;
	struct stat	info2;

	head = list;
	while (list != NULL)
	{
		comp = list->next;
		while (comp != NULL)
		{
			lstat(list->filename, &info1);
			lstat(comp->filename, &info2);
			if (info1.st_mtime < info2.st_mtime)
			{
				swap(&list->filename, &comp->filename);
				list = head;
				comp = list->next;
			}
			if (info1.st_mtime == info2.st_mtime &&
					ft_strcmp(list->filename, comp->filename) > 0)
				swap(&list->filename, &comp->filename);
			comp = comp->next;
		}
		list = list->next;
	}
}

void	trlstsort(t_files *list, t_files *head)
{
	t_files		*comp;
	struct stat	info1;
	struct stat	info2;

	head = list;
	while (list != NULL)
	{
		comp = list->next;
		while (comp != NULL)
		{
			lstat(list->filename, &info1);
			lstat(comp->filename, &info2);
			if (info1.st_mtime > info2.st_mtime)
			{
				swap(&list->filename, &comp->filename);
				list = head;
				comp = list->next;
			}
			else
				comp = comp->next;
		}
		list = list->next;
	}
}
