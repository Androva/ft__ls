/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   no_flags.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/04 16:19:23 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/10 11:37:48 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	no_flags(t_files *files)
{
	int i;

	i = 0;
	while (files != NULL)
	{
		if (i != 0 && i % 6 == 0)
			ft_putchar('\n');
		ft_padstr(files->filename, 20);
		i++;
		files = files->next;
	}
	ft_putchar('\n');
}
